CC = xelatex
ROOT = ./
RESUME_SRCS = $(shell find $(ROOT) -name '*.tex')

build: $(ROOT)/resume.tex $(RESUME_SRCS)
	$(CC) -output-directory=$(ROOT) $<

.PHONY: clean
clean:
	rm -rf resume.pdf